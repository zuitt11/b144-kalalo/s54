let collection = [];
let top = 0

// Write the queue functions below.
function print(){
	return collection
}
function enqueue(element){
	collection[top]=element
	top = top+1
	return collection
}
function front(){
	return collection[0]
}
function size(){
	return collection.length
}
function dequeue(){
	if (isEmpty() === false) {
		top = top-1
		return collection.splice(0, 1);
	}
}
function isEmpty(){
	return collection[top]===0
}
module.exports = {
print,
enqueue,
dequeue,
front,
size,
isEmpty
};